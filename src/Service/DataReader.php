<?php

namespace App\Service;

use DOMDocument;
use DOMElement;
use DOMXPath;

class DataReader
{
    public function readModal(int $day, DOMDocument $doc)
    {
        $user = $doc->documentElement->getAttributeNS('http://anne-ducat.fr', 'user');
        $xpath = new DOMXpath($doc);
        $xpath->registerNamespace('a', 'http://anne-ducat.fr');
        $modal = $xpath->query('/a:document/a:modal[@day="'.$day.'"]')->item(0);
        
        $title = (string)$xpath->evaluate('string(a:title)', $modal);
        $footer = (string)$xpath->evaluate('string(a:footer)', $modal);
        $content = $this->innerHTML($xpath->evaluate('a:content', $modal)->item(0));

        return [
            'user' => $user,
            'modal' => [
                'title' => $title,
                'content' => $content,
                'footer' => $footer
            ]
        ];
    }

    public function readDocument(DOMDocument $doc)
    {
        $user = $doc->documentElement->getAttributeNS('http://anne-ducat.fr', 'user');
        return [
            'user' => $user
        ];
    }
    
    /**
     * @see https://stackoverflow.com/questions/2087103/how-to-get-innerhtml-of-domnode/39193507#39193507
     */
    private function innerHTML(DOMElement $node) : string {
        return implode(
            array_map([$node->ownerDocument, 'saveHTML'], 
            iterator_to_array($node->childNodes)
        ));
    }
}
