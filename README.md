# Advent Calendar
### _with reals chocolate items_
![Gluten](https://img.shields.io/badge/gluten-free-g) ![Smile](https://img.shields.io/badge/make%20people-smile-orange)

Advent Calendar is a customizable advent calendar developed with PHP/Symfony.

It's easy to use and fun! You just have to fill it with your own elements and send it as a gift to your friends.\
They will love it ! 🦌 ❤️ 


## Table of Contents
1. [Description](#description)
2. [Visuals and Demo](#demo)
3. [Technologies](#technologies)
4. [Installation](#installation)
5. [Instructions](#instructions)
5. [Licence](#licence)


## Description
Advent Calendar is a light web application to show one surprise per day before Christmas, or any other event.\
Fully customizable, it is developed with PHP/Symfony and works without JS.

You can add your own elements (videos, photos, texts, musics) and format them in a standardized XML file. When you click on the window of the day, a modal will open and reveal a surprise. Several templates are available to diversify the content of these modals. 


## Visuals and Demo
![Visuals](doc/demo.gif "Visuals advent calendar project")

You can also test the [demo  🎁](https://advent-calendar.anne-ducat.fr/demo)


## Technologies
* PHP 7.2.5 ou plus
* Symfony 5.3 et [Symfony requirements](https://symfony.com/doc/current/setup.html)

## Installation
Download source file or run this command:
```bash
git clone git@gitlab.com:anneducat/advent-calendar.git
```
Install the project:
```bash
cd my_project
composer install
```

Execute these commands to launch the project in the browser:
```bash
symfony server:start -d
symfony open:local
```

## Instructions
### Data
_How to fill the calendar with your own multimedia content?_\
Import your items into `data/files`\
Format your texts and content in an .xml file. Use the syntax and the standardized templates proposed in `data/demo.xml`

### Front
_How to customize the background or other graphic elements of the template?_\
Import your items into `public/img`\
Edit .twig files in `templates`

_How to customize error pages?_\
Edit .twig files in `templates/bundles/TwigBundle/Exception`

### Back
You can modify start date of the calendar in `src/Controller/CalendarController.php` (see comments).\
You can modify the number and the content of the calendar windows (chocolate pictures) in `src/Service/CalendarConstructor.php` (see comments).\
You can change how the XML file is read in `src/Service/DataReader.php`.

## Licence
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

_Do you have a question or do you want discuss the project?_\
Contact author: [Anne Ducat](mailto:email@anne-ducat.fr)
