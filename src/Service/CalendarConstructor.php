<?php

namespace App\Service;

use DateTime;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class CalendarConstructor
{
    private $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function constructCalendar(string $key) {
        $currentDate = DateTime::createFromFormat('U', time());
        $currentDay = $currentDate->format('d');
        // For calendar until the last day of the month
        // $date = (clone $currentDate)->modify('last day of this month');
        // $lastday = $date->format('d');

        // Chocolate array
        $i =0;
        $truffes = range(1, 8);
        $truffes = array_map(function($item){
            return "truffe{$item}.png";
        },$truffes);
        $nbr_truffes = count($truffes);
        
        // For calendar until the last day of the month
        // $days = range(1,$lastday);
        // For advent calendar up to 24 ou 25 december
        $days = range(1,25);
        foreach ($days as $day) {
	    // Replace $currentDay by 25 for test or review of all calendar boxes
            if ($day <= $currentDay) {
                $doors[] = [
                    'number' => $day,
                    'href' => $this->urlGenerator->generate('modal',['day' => $day, 'key' => $key]),
                    //insert chocolate behind each door
                    'chocolate' => $truffes[$i++ % $nbr_truffes]
                ];
             } else {
                $doors[] = [
                    'number' => $day,
                    'href' => '',
                ];
            }
        }

        return ['doors' => $doors];
    }
}
