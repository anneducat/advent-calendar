<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use DateTime;
use App\Service\DataReader;
use App\Service\CalendarConstructor;
use DOMDocument;

class CalendarController extends AbstractController
{
    const FILES_PATH = "../data/files/";

    private $urlGenerator;
    private $reader;
    private $calendar;

    public function __construct(UrlGeneratorInterface $urlGenerator, DataReader $reader, CalendarConstructor $calendar)
    {
        $this->urlGenerator = $urlGenerator;
        $this->reader = $reader;
        $this->calendar = $calendar;
    }

    private function loadFile(string $key) : DOMDocument {
        $filename = "../data/{$key}.xml";
        if (!file_exists($filename)) {
            throw $this->createNotFoundException('Page not found!');
        }
        $doc = new DOMDocument();
        $doc->load($filename);
        return $doc;
    }

    /**
     * @Route("/{key}", name="calendar")
     */
    public function index(string $key): Response
    {
        $currentDate = DateTime::createFromFormat('U', time());
        // For calendar valid from specific date, update strtotime() - ex :
        // $date = DateTime::createFromFormat('U', strtotime("2021-12-01"));
        // For demo valid all year
        $date = DateTime::createFromFormat('U', strtotime("2020-12-01"));
        if ($currentDate < $date) {
            throw $this->createAccessDeniedException();
        }
        $doc = $this->loadFile($key);
        $user = $this->reader->readDocument($doc);
        return $this->render(
            'calendar/index.html.twig',
            $user + $this->calendar->constructCalendar($key)
        );
    }

    /**
     * @Route("/{key}/{day}", name="modal", requirements={"day"="\d+"})
     */
    public function show(int $day, string $key) 
    {
        $currentDate = DateTime::createFromFormat('U', time());
        // For a calendar valid for a specific month
        // $date = DateTime::createFromFormat('U', strtotime("2021-12-{$day}"));
        // if ($currentDate < $date) {
        // For demo valid all year
        if ((int)$currentDate->format('j') < $day) {
            throw $this->createAccessDeniedException();
        }
        $doc = $this->loadFile($key);
        $modal = $this->reader->readModal($day, $doc);
        return $this->render('calendar/modal.html.twig',
            $this->calendar->constructCalendar($key) + $modal +
            [ 
                'home' => $this->urlGenerator->generate('calendar', ['key' => $key])
            ]
        );
    }

    /**
     * @Route("/files/{filename}", name="files")
     */
    public function download($filename)
    {
        if (!file_exists(self::FILES_PATH.$filename)) {
            throw $this->createNotFoundException('Page not found!');
        }
        return $this->file(self::FILES_PATH.$filename);
    }

}
